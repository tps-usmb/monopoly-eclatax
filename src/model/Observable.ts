/**
 * Observable
 * Une implémentation maison du pattern Observable/Observer basée sur des évènements DOM
 * Une entité dite `Observable` peut être surveillée par un composant React
 * et ainsi notifier ses changements d'état à la vue
 * /!\ ABSTRAIT /!\
 */

class Observable {

    notifyEvent: Event
    notifyEventEl: HTMLElement


    constructor() {
        this.notifyEvent = new CustomEvent("notify")
        this.notifyEventEl = document.createElement("div")
    }

    notify() {
        this.notifyEventEl.dispatchEvent(this.notifyEvent)
    }

    observe(fn: (e: CustomEvent) => void) {
        this.notifyEventEl.addEventListener("notify", fn)
    }

    stopObserving(fn: (e: CustomEvent) => void) {
        this.notifyEventEl.removeEventListener("notify", fn)
    }

}

export default Observable
