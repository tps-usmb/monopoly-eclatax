import District from "./District"
import Game from "./Game"
import TileImage from "./images/TileImage"
import Observable from "./Observable"
import Land from "./tiles/Land"
import PublicServiceCompany from "./tiles/PublicServiceCompany"
import Station from "./tiles/Station"
import Tile from "./tiles/Tile"


/**
 * Board
 * Le plateau de jeu
 */

class Board extends Observable {

    game: Game
    tiles: Tile[]


    constructor(game: Game) {
        super()

        this.game = game

        const startTile = new Tile(this, "Départ", null)

        // Quartiers
        const brownDistrict   = new District(this, "Marron", "#260707")
        const cyanDistrict    = new District(this, "Cyan",   "#70dbff")
        const pinkDistrict    = new District(this, "Rose",   "#ff3bde")
        const orangeDistrict  = new District(this, "Orange", "#ff7f3b")
        const redDistrict     = new District(this, "Rouge",  "#ff2626")
        const yellowDistrict  = new District(this, "Jaune",  "#ffe626")
        const greenDistrict   = new District(this, "Vert",   "#72ff26")
        const blueDistrict    = new District(this, "Vert",   "#1e12ff")

        // Terrains
        const quaiJeuPaumes = new Land(
            this,
            "Quai du Jeu de Paume",
            null,
            100, 60, 150,
            brownDistrict
        )
        const avDucs = new Land(
            this,
            "Avenue des Ducs de Savoie",
            null,
            100, 60, 150,
            brownDistrict
        )

        const rueFavre = new Land(
            this,
            "Rue Favre",
            null,
            60, 40, 100,
            cyanDistrict
        )
        const plPalaisJustice = new Land(
            this,
            "Place du Palais de Justice",
            null,
            60, 40, 100,
            cyanDistrict
        )

        const rueJuiverie = new Land(
            this,
            "Rue Juiverie",
            null,
            180, 60, 50,
            pinkDistrict
        )
        const rueBoigne = new Land(
            this,
            "Rue de Boigne",
            null,
            180, 60, 50,
            pinkDistrict
        )
        const rueChâteau = new Land(
            this,
            "Rue du Château",
            null,
            200, 60, 50,
            pinkDistrict
        )

        const avLyon = new Land(
            this,
            "Avenue de Lyon",
            null,
            220, 60, 150,
            orangeDistrict
        )
        const placeCaffe = new Land(
            this,
            "Place Caffe",
            null,
            240, 60, 150,
            orangeDistrict
        )

        // Gares
        const sncf = new Station(
            this,
            "Gare SNCF de Chambéry Challes-les-Eaux",
            new TileImage("sncf.png"),
            200, 100
        )

        const gareRoutière = new Station(
            this,
            "Gare Routière du Verney",
            new TileImage("gare_routière.png"),
            200, 100
        )

        const ducs = new Station(
            this,
            "Arrêt Synchro Bus des Ducs",
            new TileImage("synchro_bus.png"),
            200, 50
        )

        const univBourget = new Station(
            this,
            "Arrêt Synchro Bus de l'université du Bourget",
            new TileImage("synchro_bus.png"),
            200, 50
        )

        // Services publics
        const serviceEaux = new PublicServiceCompany(
            this,
            "Service des Eaux du Grand Chambéry",
            new TileImage("service_eaux.png"),
            150, 70
        )

        const velostation = new PublicServiceCompany(
            this,
            "Vélostation",
            new TileImage("velostation.png"),
            150, 70
        )

        // Ordre des cases sur le plateau
        this.tiles = [
            startTile,

            quaiJeuPaumes,
            avDucs,

            sncf,

            rueFavre,
            velostation,
            plPalaisJustice,

            serviceEaux,

            rueJuiverie,
            ducs,
            rueBoigne,
            rueChâteau,

            gareRoutière,

            avLyon,
            placeCaffe,

            univBourget,
        ]
    }


    nextTile(tile: Tile, diceValue: number): Tile {
        const index = this.tiles.indexOf(tile)
        const nextIndex = (index + diceValue) % (this.tiles.length - 1)

        return this.tiles[ nextIndex ]
    }

}

export default Board
