import Board from "./Board"
import Observable from "./Observable"
import Player from "./Player"


/**
 * Game
 * Une partie
 */

class Game extends Observable {

    players: Player[]
    board: Board
    currentPlayer: Player


    constructor(players: Player[]) {
        super()

        this.players = players
        this.board = new Board(this)
    }


    // Démarrage de la partie
    start() {
        // Distribution de l'argent + placement sur la case départ
        this.players.map(player => {
            player.money = 1500
            player.moveTo(this.board.tiles[ 0 ])
        })

        // Choix du premier joueur
        this.currentPlayer = this.players[ 0 ]

        // Notifier la vue
        this.notify()
    }

    nextPlayer() {
        const index = this.players.indexOf(this.currentPlayer)

        if (index === this.players.length - 1)
            this.currentPlayer = this.players[ 0 ]
        else
            this.currentPlayer = this.players[ index + 1 ]

        this.notify()
    }

    play(diceValue: number) {
        this.currentPlayer.play(diceValue)
        this.notify()
    }

}

export default Game
