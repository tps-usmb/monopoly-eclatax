import Board from "./Board"


/**
 * District
 * Un quartier contenant plusieurs cases
 */

class District {

    board: Board
    name: string
    color: string


    constructor(board: Board, name: string, color: string) {
        this.board = board
        this.name  = name
        this.color = color
    }

}

export default District
