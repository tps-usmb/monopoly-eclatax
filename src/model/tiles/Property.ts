import Board from "../Board"
import TileImage from "../images/TileImage"
import Player from "../Player"
import FreePropertyState from "./states/FreePropertyState"
import OwnedPropertyState from "./states/OwnedPropertyState"
import PropertyState from "./states/PropertyState"
import Tile from "./Tile"


/**
 * Property
 * Une case pouvant être achetée par un joueur
 * /!\ ABSTRAIT /!\
 */

class Property extends Tile {

    price: number
    taxes: number
    state: PropertyState


    constructor(board: Board, name: string, image: TileImage | null, price: number, taxes: number) {
        super(board, name, image)

        this.price = price
        this.taxes = taxes
        this.state = new FreePropertyState(this)
    }


    getOwner(): Player {
        return this.state.owner
    }

    owned(): boolean {
        return this.state.owned()
    }

    ownedBy(player: Player): boolean {
        return this.state.ownedBy(player)
    }

    markOwnedBy(player: Player) {
        this.state = new OwnedPropertyState(this, player)
        this.notify()
    }


    taxesToPay(): number {
        return this.state.taxesToPay()
    }


    onPlayerArrival(player: Player) {
        this.state.onPlayerArrival(player)
    }

}

export default Property
