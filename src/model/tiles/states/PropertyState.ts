import Player from "../../Player"
import Property from "../Property"


/**
 * PropertyState
 * L'état d'une propriété
 * /!\ ABSTRAIT /!\
 */

class PropertyState {

    property: Property
    owner: Player | null


    constructor(property: Property) {
        this.property = property
    }


    owned(): boolean {
        return this.owner !== null
    }

    ownedBy(player: Player): boolean {
        return player === this.owner
    }


    taxesToPay(): number {
        return this.property.taxes
    }


    onPlayerArrival(player: Player) {
        if (this.owned() && !this.ownedBy(player)) {
            player.pay(this.taxesToPay(), this.owner)

            player.notify()
            this.owner.notify()
        }
    }

}

export default PropertyState
