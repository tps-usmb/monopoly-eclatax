import Property from "../Property"
import LandState from "./LandState"


/**
 * FreeLandState
 * État d'un terrain indiquant qu'il est libre
 */

class FreeLandState extends LandState {

    constructor(property: Property) {
        super(property)

        this.owner = null
    }

}

export default FreeLandState
