import Player from "../../Player"
import Land from "../Land"
import Property from "../Property"
import LandState from "./LandState"


/**
 * ConstructibleLandState
 * État d'un terrain indiquant le fait qu'il est constructible
 */

class ConstructibleLandState extends LandState {

    constructor(property: Property, owner: Player) {
        super(property)

        this.owner = owner
        this.buildings = 0
    }


    canBuild(): boolean {
        return this.buildings < Land.maxBuildings
            && (this.property as Land).buildPrice <= this.owner.money
    }

    build() {
        if (!this.canBuild()) return

        this.buildings++
        this.owner.money -= (this.property as Land).buildPrice

        this.property.notify()
        this.owner.notify()
    }


    taxesToPay(): number {
        return super.taxesToPay() * (this.buildings + 1)
    }

}

export default ConstructibleLandState
