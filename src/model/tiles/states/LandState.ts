import Property from "../Property"
import PropertyState from "./PropertyState"


/**
 * LandState
 * L'état d'un terrain
 * /!\ ABSTRAIT /!\
 */

class LandState extends PropertyState {

    buildings: number


    constructor(property: Property) {
        super(property)
    }


    canBuild(): boolean {
        return false
    }

    build() {
    }

}

export default LandState
