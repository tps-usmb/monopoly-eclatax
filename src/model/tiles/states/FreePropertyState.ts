import Property from "../Property"
import PropertyState from "./PropertyState"


/**
 * FreePropertyCase
 * État d'une propriété indiquant le fait qu'elle soit libre
 */

class FreePropertyState extends PropertyState {

    constructor(property: Property) {
        super(property)

        this.owner = null
    }

}

export default FreePropertyState
