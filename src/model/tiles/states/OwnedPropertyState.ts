import Player from "../../Player"
import Property from "../Property"
import PropertyState from "./PropertyState"


/**
 * FreePropertyCase
 * État d'une propriété indiquant le fait qu'elle ait été achetée
 */

class OwnedPropertyState extends PropertyState {

    constructor(property: Property, owner: Player) {
        super(property)

        this.owner = owner
    }

}

export default OwnedPropertyState
