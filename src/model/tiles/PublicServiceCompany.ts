import Board from "../Board"
import TileImage from "../images/TileImage"
import Property from "./Property"


/**
 * PublicServiceCompany
 * Une companie du service public
 */

class PublicServiceCompany extends Property {

    constructor(board: Board, name: string, image: TileImage | null, price: number, taxes: number) {
        super(board, name, image, price, taxes)
    }

}

export default PublicServiceCompany
