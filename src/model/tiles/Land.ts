import Board from "../Board"
import District from "../District"
import TileImage from "../images/TileImage"
import Player from "../Player"
import Property from "./Property"
import ConstructibleLandState from "./states/ConstructibleLandState"
import FreeLandState from "./states/FreeLandState"
import LandState from "./states/LandState"


/**
 * Land
 * Une case de terrain
 */

class Land extends Property {

    static maxBuildings: number = 4

    buildPrice: number
    district: District


    constructor(board: Board, name: string, image: TileImage | null, price: number, taxes: number, buildPrice: number, district: District) {
        super(board, name, image, price, taxes)

        this.buildPrice = buildPrice
        this.district = district

        this.state = new FreeLandState(this)
    }


    markOwnedBy(player: Player) {
        this.state = new ConstructibleLandState(this, player)
        this.notify()
    }


    getBuildings(): number {
        return (this.state as LandState).buildings
    }

    canBuild(): boolean {
        return (this.state as LandState).canBuild()
    }

    build() {
        return (this.state as LandState).build()
    }

}

export default Land
