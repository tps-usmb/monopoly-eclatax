import Board from "../Board"
import TileImage from "../images/TileImage"
import Observable from "../Observable"
import Player from "../Player"


/**
 * Tile
 * Une case du plateau
 * /!\ ABSTRAIT /!\
 */

class Tile extends Observable {

    static ViewComponent: (props: { tile: Tile }) => JSX.Element | null  // Le composant React de vue associé à ce modèle

    board: Board
    name: string
    image: TileImage | null


    constructor(board: Board, name: string, image: TileImage | null) {
        super()

        this.board = board
        this.name  = name
        this.image = image
    }


    // Déclenché quand un joueur arrive sur la case
    onPlayerArrival(player: Player) {
    }

}

export default Tile
