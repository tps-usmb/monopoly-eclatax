import Image from "./Image"


/**
 * TileImage
 * Une image pouvant être affichée sur une case
 */

class TileImage extends Image {

    constructor(fileName: string) {
        super(fileName, "tiles/")
    }

}

export default TileImage
