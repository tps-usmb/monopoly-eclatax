/**
 * Image
 * Une image pouvant être affichée par une vue
 */

class Image {

    static directory: string = "images/"

    fileName: string
    subDirectory: string


    constructor(fileName: string, subDirectory: string) {
        this.fileName = fileName
        this.subDirectory = subDirectory
    }

    getUri() {
        return Image.directory + this.subDirectory + this.fileName
    }

}

export default Image
