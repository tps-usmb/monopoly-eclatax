import Image from "./Image"


/**
 * Avatar
 * Un avatar pouvant être porté par un joueur
 */

class Avatar extends Image {

    constructor(fileName: string) {
        super(fileName, "avatars/")
    }

}

export default Avatar
