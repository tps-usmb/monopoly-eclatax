import Game from "./Game"
import Avatar from "./images/Avatar"
import Observable from "./Observable"
import Property from "./tiles/Property"
import Tile from "./tiles/Tile"


/**
 * Player
 * Un joueur de la partie
 */

class Player extends Observable {

    game: Game

    name: string
    avatar: Avatar

    money: number
    currentTile: Tile


    constructor(game: Game, name: string, avatar: Avatar) {
        super()

        this.game = game
        this.name = name
        this.avatar = avatar
    }


    play(diceValue: number) {
        this.moveTo( this.game.board.nextTile( this.currentTile, diceValue ) )
        this.currentTile.onPlayerArrival(this)
    }


    moveTo(tile: Tile) {
        this.currentTile = tile
        this.notify()
    }

    buyCurrentTile() {
        if (!(this.currentTile instanceof Property)) return
        if (this.currentTile.owned()) return
        if (this.currentTile.price > this.money) return

        this.money -= this.currentTile.price
        this.currentTile.markOwnedBy(this)

        this.notify()
    }


    isBankrupt() {
        return this.money <= 0
    }

    pay(amount: number, to: Player) {
        this.money -= amount
        to.money += amount

        this.notify()
        to.notify()
    }

}

export default Player
