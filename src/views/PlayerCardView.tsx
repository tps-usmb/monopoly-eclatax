import { Avatar, Stack, Typography } from "@mui/material"
import React from "react"
import Game from "../model/Game"
import Player from "../model/Player"
import observe from "./observe"


type PlayerCardViewProps = {
    game: Game
    player: Player
}


export default function PlayerCardView(props: PlayerCardViewProps) {

    const {
        game,
        player,
    } = props
    observe(player)


    return (
        <Stack
            direction="column"
            alignItems="flex-start"
            spacing={2}
        >
            <Typography>Joueur courant :</Typography>

            <Stack
                direction="row"
                alignItems="center"
                spacing={2}
            >
                <Avatar
                    src={player.avatar.getUri()}
                    sx={{
                        width: 96,
                        height: 96,
                    }}
                />

                <Stack
                    direction="column"
                    alignItems="flex-start"
                    spacing={2}
                >
                    <Typography variant="h4">{ player.name }</Typography>
                    <Typography variant="h4">{ player.money } €</Typography>
                </Stack>
            </Stack>
        </Stack>
    )

}
