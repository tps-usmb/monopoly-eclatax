import React from "react"
import Tile from "../model/tiles/Tile"
import Game from "../model/Game"
import observe from "./observe"
import { Button, Stack, Typography } from "@mui/material"
import Property from "../model/tiles/Property"
import { useSnackbar } from "notistack"
import Land from "../model/tiles/Land"


type TileCardViewProps = {
    game: Game
    tile: Tile
}


export default function TileCardView(props: TileCardViewProps) {

    const {
        game,
        tile,
    } = props
    observe(tile)


    const { enqueueSnackbar } = useSnackbar()


    return (
        <Stack
            direction="column"
            alignItems="flex-start"
            spacing={1}

            sx={{
                width: "fit-content",
            }}
        >
            <Typography variant="h4">{ tile.name }</Typography>

            {tile instanceof Property && <>
                {tile.ownedBy(game.currentPlayer) ? <>
                    <Typography variant="body1">Vous êtes le propriétaire de cette case.</Typography>

                    {tile instanceof Land && tile.canBuild() &&
                        <Button
                            variant="contained"
                            onClick={() => tile.build()}
                        >
                            Construire ({ tile.buildPrice } €)
                        </Button>
                    }
                </> : <>
                    {tile.owned() ?
                        <Typography variant="body1">Appartient à { tile.getOwner().name }</Typography>
                    :
                        <Stack
                            direction="row"
                            alignItems="center"
                            spacing={2}
                        >
                            <Typography variant="h5">{ tile.price } €</Typography>

                            <Button
                                variant="contained"
                                onClick={() => {
                                    if (tile.price > game.currentPlayer.money)
                                        enqueueSnackbar("Vous n'avez pas assez d'argent !", { variant: "error" })

                                    game.currentPlayer.buyCurrentTile()
                                }}
                            >
                                Acheter
                            </Button>
                        </Stack>
                    }
                </>}
            </>}
        </Stack>
    )

}
