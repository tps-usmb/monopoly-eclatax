import { useEffect, useState } from "react"
import Observable from "../model/Observable"


export default function observe(obj: Observable) {

    const updatedAtState = useState<Date>(new Date())
    const [, setUpdatedAt] = updatedAtState

    useEffect(() => {
        function onNotify(e: CustomEvent) {
            setUpdatedAt(new Date())
        }

        obj.observe(onNotify)

        return function cleanup() {
            obj.stopObserving(onNotify)
        }
    }, [obj])

    return updatedAtState

}
