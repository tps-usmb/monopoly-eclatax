import { Box } from "@mui/system"
import React, { useEffect, useState } from "react"
import Game from "../model/Game"
import Board from "../model/Board"
import observe from "./observe"
import TileView from "./TileView"


export type BoardViewProps = {
    game: Game
    board: Board
}


export default function BoardView(props: BoardViewProps) {

    const { game, board } = props
    observe(board)

    const [sideLength, setSideLength] = useState<Number | null>(null)
    useEffect(() => {
        setSideLength(Math.ceil((board.tiles.length + 4) / 4))
    }, [board])


    if (!sideLength) return <></>

    let tileSide = 1
    let tileColumn = 1
    let tileRow = 1

    return (
        <Box
            sx={{
                width: "100%",
                height: "100%",
                overflow: "scroll",
            }}
        >
            <Box
                sx={{
                    width: "fit-content",
                    height: "fit-content",

                    backgroundColor: "#cde7ce",

                    display: "grid",
                    gridTemplateColumns: `repeat(${ sideLength }, 1fr)`,
                    gridTemplateRows: `repeat(${ sideLength }, 1fr)`,
                    gap: 1,
                }}
            >
                {board.tiles.map((tile, key) => {
                    const el = <TileView
                        key={key}

                        game={game}
                        tile={tile}

                        column={tileColumn}
                        row={tileRow}
                    />

                    switch (tileSide) {
                        case 1:
                            if (tileColumn === sideLength) {
                                tileSide = 2
                                tileRow++
                            }
                            else
                                tileColumn++

                            break

                        case 2:
                            if (tileRow === sideLength) {
                                tileSide = 3
                                tileColumn--
                            }
                            else
                                tileRow++

                            break

                        case 3:
                            if (tileColumn === 1) {
                                tileSide = 4
                                tileRow--
                            }
                            else
                                tileColumn--

                            break

                        case 4:
                            tileRow--
                            break
                    }

                    return el
                })}
            </Box>
        </Box>
    )

}
