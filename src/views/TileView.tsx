import { Avatar as MuiAvatar, Stack, Tooltip, Typography } from "@mui/material"
import { Box } from "@mui/system"
import React from "react"
import Player from "../model/Player"
import Game from "../model/Game"
import Land from "../model/tiles/Land"
import Property from "../model/tiles/Property"
import Tile from "../model/tiles/Tile"
import observe from "./observe"


export type TileViewProps = {
    game: Game
    tile: Tile
    column: Number
    row: Number
}


export default function TileView(props: TileViewProps) {

    const {
        game,
        tile,
        column,
        row,
    } = props

    // La vue d'une case l'observe elle plus tous les joueurs (si un joueur se déplace sur la case il faut rafraîchir la vue)
    observe(tile)

    game.players.map(player =>
        observe(player)
    )


    function playersOnTile(): Player[] {
        return game.players.filter(player =>
            player.currentTile === tile
        )
    }


    return (
        <Box
            sx={{
                width: "300px",
                height: "400px",

                border: "5px solid #000000",

                gridColumn: column.toString(),
                gridRow: row.toString(),
            }}
        >
            <Stack
                direction="column"
                justifyContent="space-between"
                alignItems="center"
                spacing={2}

                sx={{
                    width: "100%",
                    height: "100%",
                }}
            >
                {tile instanceof Land &&
                    <Box
                        sx={{
                            width: "100%",
                            height: "50px",
                            backgroundColor: tile.district.color,

                            borderBottom: "5px solid #000000",
                        }}
                    >
                        <Stack
                            direction="row"
                            justifyContent="flex-start"
                            alignItems="center"
                            spacing={1}

                            sx={{
                                width: "100%",
                                height: "100%",
                            }}
                        >
                            {tile.getBuildings() > 0 && [...Array(tile.getBuildings())].map(key =>
                                <Box
                                    key={key}
                                    sx={{
                                        width: "30px",
                                        height: "30px",

                                        border: "2px solid #000000",
                                        backgroundColor: "green",
                                    }}
                                />
                            )}
                        </Stack>
                    </Box>
                }

                <Stack
                    direction="row"
                    justifyContent="flex-start"
                    alignItems="center"
                    spacing={1}
                >
                    {playersOnTile().map((player, key) =>
                        <Tooltip key={key} title={player.name}>
                            <MuiAvatar
                                src={player.avatar.getUri()}

                                sx={{
                                    width: 56,
                                    height: 56,
                                }}
                            />
                        </Tooltip>
                    )}
                </Stack>

                <Typography variant="h5" align="center">{ tile.name }</Typography>

                {tile instanceof Property &&
                    <Typography variant="h5">{ tile.price } €</Typography>
                }
            </Stack>
        </Box>
    )

}
