import { Casino, Close, PlayArrow } from "@mui/icons-material"
import { AppBar, Button, Container, Dialog, DialogContent, IconButton, Paper, Stack, TextField, Tooltip, Typography } from "@mui/material"
import { Box } from "@mui/system"
import { useSnackbar } from "notistack"
import React, { useEffect, useState } from "react"
import { useLocation, useNavigate } from "react-router"
import Game from "./model/Game"
import Avatar from "./model/images/Avatar"
import Player from "./model/Player"
import { PlayerDescriptor } from "./PlayersScreen"
import BoardView from "./views/BoardView"
import observe from "./views/observe"
import PlayerCardView from "./views/PlayerCardView"
import TileCardView from "./views/TileCardView"


export default function GameScreen(props: Object) {

    const navigate = useNavigate()
    const location = useLocation()
    const state = location.state as { players: PlayerDescriptor[] }

    const { enqueueSnackbar } = useSnackbar()


    // Création initiale de la partie
    const [game, setGame] = useState<Game>(
        function(): Game {
            const game = new Game([])
            game.players = state.players.map(player =>
                new Player(game, player.name, new Avatar(player.avatar.fileName))
            )

            return game
        }()
    )

    // La vue observe l'objet de la partie
    observe(game)


    const [mustRollDice, setMustRollDice] = useState<boolean>(false)
    const [diceDialogOpen, setDiceDialogOpen] = useState<boolean>(false)
    const [diceValue, setDiceValue] = useState<number>(1)
    const [mustPlay, setMustPlay] = useState<boolean>(false)


    // Monitoring du tour des joueurs
    useEffect(() => {
        if (game.currentPlayer) {
            enqueueSnackbar("C'est au tour de " + game.currentPlayer.name + " de jouer !")
            setMustRollDice(true)
            setMustPlay(false)
        }
    }, [game.currentPlayer])


    // Démarrage de la partie
    useEffect(() => {
        game.start()
    }, [game])


    return <>
        <Stack
            direction="column"
            alignItems="stretch"

            sx={{
                width: "100%",
                height: "100%",
            }}
        >
            <AppBar position="static">
                <Container>
                    <Stack
                        direction="row"
                        justifyContent="space-between"
                        alignItems="center"
                    >
                        <Stack
                            direction="row"
                            alignItems="flex-end"
                            spacing={1}
                        >
                            <Typography variant="h4">Monopoly</Typography>
                            <Typography variant="body1">E C L A T A X</Typography>
                        </Stack>

                        <Tooltip title="Terminer la partie">
                            <IconButton
                                onClick={() => navigate("/")}
                            >
                                <Close />
                            </IconButton>
                        </Tooltip>
                    </Stack>
                </Container>
            </AppBar>

            <Box
                sx={{
                    flexGrow: 1,
                    overflow: "hidden",
                }}
            >
                <BoardView
                    game={game}
                    board={game.board}
                />
            </Box>

            <Box
                sx={{
                    position: "absolute",
                    bottom: 12,
                    left: 12,
                }}
            >
                <Paper>
                    <Stack
                        direction="row"
                        alignItems="center"
                        spacing={2}
                    >
                        {mustRollDice &&
                            <Tooltip title="Lancer le dé">
                                <IconButton
                                    onClick={() => setDiceDialogOpen(true)}
                                >
                                    <Casino />
                                </IconButton>
                            </Tooltip>
                        }

                        {mustPlay &&
                            <Tooltip title="Prochain tour">
                                <IconButton
                                    onClick={() => game.nextPlayer()}
                                >
                                    <PlayArrow />
                                </IconButton>
                            </Tooltip>
                        }
                    </Stack>
                </Paper>
            </Box>

            {game.currentPlayer &&
                <Box
                    sx={{
                        position: "absolute",
                        right: 12,
                        top: 48,

                        width: "fit-content",
                    }}
                >
                    <Paper sx={{ padding: 2 }}>
                        <PlayerCardView
                            game={game}
                            player={game.currentPlayer}
                        />
                    </Paper>
                </Box>
            }

            {mustPlay &&
                <Box
                    sx={{
                        position: "absolute",
                        right: 12,
                        bottom: 12,

                        width: "fit-content",
                    }}
                >
                    <Paper sx={{ padding: 2 }}>
                        <TileCardView
                            game={game}
                            tile={game.currentPlayer.currentTile}
                        />
                    </Paper>
                </Box>
            }
        </Stack>

        <Dialog
            open={diceDialogOpen}
            onClose={() => setDiceDialogOpen(false)}
        >
            <DialogContent>
                <TextField
                    label="Valeur du dé"
                    type="number"

                    value={diceValue}
                    onChange={e => setDiceValue(parseInt(e.target.value))}
                />

                <Button
                    onClick={() => {
                        game.play(diceValue)

                        setDiceDialogOpen(false)
                        setDiceValue(1)

                        setMustRollDice(false)
                        setMustPlay(true)
                    }}
                >
                    Jouer
                </Button>
            </DialogContent>
        </Dialog>
    </>

}
