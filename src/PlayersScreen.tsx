import { useTheme } from "@emotion/react"
import { Avatar as MuiAvatar, Button, Chip, Paper, Stack, TextField, Typography } from "@mui/material"
import { useSnackbar } from "notistack"
import React, { useState } from "react"
import { useNavigate } from "react-router"
import Avatar from "./model/images/Avatar"


const avatars = [
    new Avatar("avatar1.png"),
    new Avatar("avatar2.png"),
    new Avatar("avatar3.png"),
    new Avatar("avatar4.png"),
    new Avatar("avatar5.png"),
    new Avatar("avatar6.png"),
    new Avatar("avatar7.png"),
    new Avatar("avatar8.jpg"),
    new Avatar("avatar9.png"),
    new Avatar("avatar10.png"),
    new Avatar("avatar11.png"),
]


export type PlayerDescriptor = {
    name: string
    avatar: Avatar
}


export default function PlayersScreen(props: Object) {

    const navigate = useNavigate()
    const { enqueueSnackbar } = useSnackbar()

    const [players, setPlayers] = useState<PlayerDescriptor[]>([])

    const [name, setName] = useState<string>("")
    const [avatar, setAvatar] = useState<Avatar>(avatars[ 0 ])


    const theme: any = useTheme()

    return (
        <Stack
            direction="column"
            justifyContent="center"
            alignItems="center"
            spacing={4}

            sx={{
                width: "100%",
                height: "100%",
            }}
        >
            <div>
                <Typography variant="h2" align="center">Monopoly</Typography>
                <Typography variant="h5" align="right">E C L A T A X</Typography>
            </div>

            <div>
                <Typography variant="h5" align="center" sx={{ marginBottom: 1 }}>Rejoindre</Typography>

                <Paper sx={{ padding: 2 }}>
                    <Stack
                        direction="row"
                        justifyContent="center"
                        alignItems="center"
                        spacing={6}
                    >
                        <Stack
                            direction="column"
                            justifyContent="center"
                            alignItems="center"
                            spacing={2}
                        >
                            <TextField
                                label="Nom"
                                value={name}
                                onChange={e => setName(e.target.value)}
                            />

                            <Stack
                                direction="row"
                                justifyContent="center"
                                alignItems="center"
                                spacing={1}
                            >
                                {avatars.map((av, key) => {
                                    const selected = av === avatar

                                    return <MuiAvatar
                                        key={key}
                                        src={av.getUri()}

                                        onClick={() => setAvatar(av)}

                                        sx={{
                                            width: 56,
                                            height: 56,
                                            cursor: "pointer",

                                            border: selected ? ("3px solid " + theme.palette.primary.main) : null,
                                        }}
                                    />
                                })}
                            </Stack>
                        </Stack>

                        <Button
                            variant="contained"
                            onClick={() => {
                                setPlayers([...players,
                                    {name, avatar},
                                ])

                                setName("")
                                setAvatar(avatars[ 0 ])
                            }}
                        >
                            Ok
                        </Button>
                    </Stack>
                </Paper>
            </div>

            <div>
                <Typography variant="h5" align="center" sx={{ marginBottom: 1 }}>Joueurs</Typography>

                <Paper sx={{ padding: 2 }}>
                    <Stack
                        direction="column"
                        justifyContent="center"
                        alignItems="center"
                        spacing={1}
                    >
                        {players.map((player, key) =>
                            <Chip
                                key={key}
                                avatar={<MuiAvatar src={player.avatar.getUri()} />}
                                label={player.name}
                                onDelete={() => setPlayers([
                                    ...players.slice(0, key),
                                    ...players.slice(key + 1),
                                ])}
                            />
                        )}
                    </Stack>
                </Paper>
            </div>

            <Button
                variant="contained"
                onClick={() => {
                    if (players.length >= 2)
                        navigate("/game", { state: { players } })
                    else
                        enqueueSnackbar("Il faut au moins deux joueurs pour lancer la partie.", { variant: "error" })
                }}
            >
                Lancer la partie
            </Button>
        </Stack>
    )

}
