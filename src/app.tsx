import CssBaseline from "@mui/material/CssBaseline"
import { createTheme, ThemeProvider } from "@mui/material/styles"
import { SnackbarProvider } from "notistack"
import React from "react"
import ReactDOM from "react-dom"
import { HashRouter, Route, Routes } from "react-router-dom"
import GameScreen from "./GameScreen"
import PlayersScreen from "./PlayersScreen"


const theme = createTheme({

})


function App(props: any) {

    return (
        <ThemeProvider theme={theme}>
            <SnackbarProvider
                anchorOrigin={{ horizontal: "left", vertical: "top" }}
                maxSnack={3}
            >
                <CssBaseline />

                <HashRouter>
                    <Routes>
                        <Route path="/" element={<PlayersScreen />} />
                        <Route path="/game" element={<GameScreen />} />
                    </Routes>
                </HashRouter>
            </SnackbarProvider>
        </ThemeProvider>
    )

}


ReactDOM.render(
    <App />,
    document.querySelector("#app")
)
