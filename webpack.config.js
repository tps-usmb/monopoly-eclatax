const path = require("path")
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin")


module.exports = (env, argv) => {
    const mode = argv.mode === "production" ? "production" : "development"

    return {
        mode,

        entry: {
            app: "./src/app",
        },
        output: {
            filename: "[name].js",
            path: path.resolve(__dirname, "dist"),
        },
        resolve: {
            extensions: [".ts", ".tsx", ".js", ".jsx", ".json"],
        },

        devtool: mode === "development" && "eval-source-map",
        devServer: {
            port: 9000,
            hot: true,

            devMiddleware: {
                writeToDisk: true,
            },

            client: {
                webSocketURL: "ws://localhost:9000/ws",
            },
        },

        module: {
            rules: [
                // {
                //     test: /\.[tj]sx?$/i,
                //     include: path.resolve(__dirname, "src"),
                //     loader: "babel-loader",
                //     options: {
                //         plugins: [
                //             mode === "development" && require.resolve("react-refresh/babel"),
                //         ].filter(Boolean),
                //     },
                // },

                {
                    test: /\.tsx?$/i,
                    include: path.resolve(__dirname, "src"),
                    loader: "ts-loader",
                },

                {
                    test: /\.css$/i,
                    use: [
                        "style-loader",
                        "css-loader",
                    ],
                },
            ],
        },

        plugins: [
            mode === "development" && new ReactRefreshWebpackPlugin(),
        ].filter(Boolean),
    }
}
